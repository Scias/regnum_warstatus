'''
Documentation, License etc.

@package regnum_warstatus
'''

# TODO : TDM, GEM AFTERMATH

from os import path, makedirs
from datetime import timedelta, datetime
from yaml import load, load_all, dump
from sys import exit
from re import findall
from requests import get, post
from time import sleep, time
from sched import scheduler
from sys import version_info
#from collections import deque

if version_info[:2] < (3, 6):
    raise Exception("Python 3.6 or a more recent version is required.")

WATCHURL = "https://www.championsofregnum.com/index.php?l=1&ref=gmf&sec=3&world="
WORLDS = ("haven","ra")
REALMS = ("Alsius","Ignis","Syrtis")

DIR = path.dirname(path.realpath(__file__))
CFG_DIR = DIR + "/cfg/"
CACHE_DIR = DIR + "/cache/"

FILES = {}
FILES["cfg"] = {
    "servers" : CFG_DIR + "servers.yaml",
    "boss" : CFG_DIR + "boss.yaml",
    "tdm" : CFG_DIR + "tdm.yaml"
        }

FORTS = {}
FORTS["Alsius"] = ["Imperia","Aggersborg","Trelleborg","Alsius Great Wall"]
FORTS["Ignis"] = ["Menirah","Samal","Shaanarid","Ignis Great Wall"]
FORTS["Syrtis"] = ["Algaros","Herbred","Eferias","Syrtis Great Wall"]
FORT_INDEXES = FORTS["Alsius"] + FORTS["Ignis"] + FORTS["Syrtis"]

BOSSES = ("Daen Rha","Evendim","Thorkul")
GEMINDEX = { "1" : "Ignis" , "2" : "Alsius" , "3" : "Syrtis" }
REALMINDEX = { "0" : "Alsius" , "6" : "Ignis" , "12" : "Syrtis" }

CUR_DATE = datetime.utcnow().replace(second=0,microsecond=0)
NEXT_RUN = CUR_DATE + timedelta(minutes=1)


class Server:
    def __init__(self, data):
        self.name = data["name"]
        self.emojis = data["emojis"]
        self.webhooks = data["webhooks"]
        

class Config:
    def __init__(self):
        for file in FILES["cfg"].values():
            if not path.exists(file):
                exit("Error")

        if not path.exists(CACHE_DIR):
            makedirs(CACHE_DIR, exist_ok=True)

    def load(self):
        self.servers = []
        with open(FILES["cfg"]["servers"]) as file:
            for data in load_all(file):
                self.servers.append(Server(data))

        with open(FILES["cfg"]["tdm"]) as file:
            self.tdmtimes = load(file)

        with open(FILES["cfg"]["boss"]) as file:
            self.bosstimes = load(file)

    def update_boss(self, boss, respawn):
        self.bosstimes[boss] = respawn.strftime("%d/%m/%Y %H:%M")
        with open(FILES["cfg"]["boss"], "w") as file:
            dump(self.bosstimes, file, default_flow_style=False)


class Cache:
    def __init__(self):
        self.new = { "html" : '' , "forts" : '' , "gems" : '' , "relics" : '' }
        
    def fill(self,  r):
        s = r.text.splitlines()
        self.new["html"] = ''.join(s[275:369])

    def parse(self):
        self.new["forts"] = findall("images/common/keep_(syrtis|ignis|alsius).gif", self.new["html"])
        for i, realm in enumerate(self.new["forts"]):
            self.new["forts"][i] = realm.capitalize()
        self.new["gems"] = findall("gem_([0-3])", self.new["html"])
        self.new["relics"] = findall("([A-z]*) relic", self.new["html"])

        if len(self.new["forts"]) != 12 or len(self.new["gems"]) != 18:
            return(False)
        else:
            return(True)

    def has_changed(self):
        return False if self.old["html"] == self.new["html"] else True

    def get_changes(self):
        changes = {}

        if self.new["forts"] != self.old["forts"]:
            cap_realms = []
            cap_forts = []

            for i, (o, n) in enumerate(zip(self.old["forts"], self.new["forts"])):
                if o != n:
                    cap_realms.append(n)
                    cap_forts.append(FORT_INDEXES[i])

            changes["captures"] = (cap_realms, cap_forts)

        if self.new["relics"] != self.old["relics"]:
            vuln_realms = []
            safe_realms = []

            for realm in REALMS:
                if not set(FORTS[realm][:3]) <= set(self.new["relics"]):
                    vuln_realms.append(realm)
                else:
                    safe_realms.append(realm)

            changes["relics"] = (vuln_realms, safe_realms)

        if self.new["gems"] != self.old["gems"]:
            gains = []
            losses = []

            for r in range(0,13,6):
                oldr = self.old["gems"][r:r+6]
                newr = self.new["gems"][r:r+6]

                if oldr != newr:
                    for i, (o, n) in enumerate(zip(oldr, newr)):
                        if o != n:
                            gem_index = 1 if i < 3 else 2
                            if n == '0':
                                losses.append((REALMINDEX[str(r)], o, gem_index))
                            else:
                                gains.append((REALMINDEX[str(r)], n, gem_index))

            gemchanges = []
            for gain in gains:
                for loss in losses:
                    if gain[1:] == loss[1:]:
                        gemchanges.append((gain[0], gain[1], gain[2], loss[0]))
                        losses.remove(loss)

            changes["gems"] = (gemchanges)

        return(changes)

    def swap(self):
        self.old = self.new.copy()


class Realm:
    def __init__(self, name, forts, gems, relics):
        self.name = name
        self.forts = []
        for fort in forts:
            self.forts.append((fort, None))
        self.gems = gems
        self.relics = relics
        
        if f"{self.name} Great Wall" not in forts:
            self.is_invaded = True
        else:
            self.is_invaded = False
        
        if not self.is_invaded and not set(FORTS[name][:3]).issubset(self.relics):
            self.is_vulnerable = True
        else:
            self.is_vulnerable = False
            
        self.may_wish = True if len(self.gems) == 6 else False
        self.last_wish = None

    def addfort(self, fort):
        self.forts.append((fort, int(datetime.utcnow().timestamp())))
        
    def delfort(self, fort):
        for i, f in enumerate(self.forts):
            if f[0] == fort:
                self.forts.pop(i)
                break
            

class Map:
    def __init__(self, cache: Cache):

        self.realm = {}

        for i, realm in enumerate(REALMS):
            forts = []
            gems = []
            relics = []

            for owner, fort in zip(cache.new["forts"], FORT_INDEXES):
                if realm == owner:
                    forts.append(fort)

            rlmgems = cache.new["gems"][i*6:i*6+6]
            for gem in rlmgems:
                if gem != '0':
                    gems.append(gem)

            for relic in cache.new["relics"]:
                if relic in FORTS[realm]:
                    relics.append(relic)

            self.realm[realm] = Realm(realm, forts, gems, relics)


    def update(self, changes):
        eventlist = []
        for event in changes:
            if event == "captures":
                realms, forts = changes["captures"]
                for realm, fort in zip(realms, forts):
                    self.realm[realm].addfort(fort)
                    for lose_realm in REALMS:
                        if lose_realm != realm:
                            for f in self.realm[lose_realm].forts:
                                if fort == f[0]:
                                    self.realm[lose_realm].delfort(fort)
                                    break
                    for fortrealm in REALMS:
                        if any(fort in f for f in FORTS[fortrealm]):
                            break
                    if "Great Wall" not in fort:
                        captype = "cap"
                    else:
                        if realm in fort:
                            captype = "rec"
                            self.realm[realm].is_invaded = False
                        else:
                            captype = "inv"
                            self.realm[fortrealm].is_invaded = True
                            self.realm[fortrealm].is_vulnerable = False
                    eventlist.append((captype, realm, fort, fortrealm))
            elif event == "relics":
                vulnrealms, saferealms = changes["relics"]
                for vuln_realm in vulnrealms:
                    if not self.realm[vuln_realm].is_vulnerable:
                        if not self.realm[vuln_realm].is_invaded:
                            self.realm[vuln_realm].is_vulnerable = True
                            eventlist.append(("vuln", vuln_realm))
                for safe_realm in saferealms:
                    if self.realm[safe_realm].is_vulnerable:
                        self.realm[safe_realm].is_vulnerable = False
            elif event == "gems":
                gemevents = []
                for gemchange in changes["gems"]:
                    w_realm, gem, index, l_realm = gemchange
                    self.realm[w_realm].gems.append(gem)
                    self.realm[l_realm].gems.remove(gem)
                    gemevents.append(("gem", w_realm, GEMINDEX[gem], index, l_realm))
                if self.realm[w_realm].may_wish:
                    if self.realm["Alsius"].gems == ["2","2"] and self.realm["Ignis"].gems == ["1","1"] and self.realm["Syrtis"].gems == ["3","3"]:
                        eventlist.append(("wish", w_realm))
                        self.realm[w_realm].last_wish = int(datetime.utcnow().timestamp())
                    self.realm[w_realm].may_wish = False
                else:
                    if len(self.realm[w_realm].gems) == 6:
                        self.realm[w_realm].may_wish = True
                    eventlist += gemevents
        return(eventlist)
    
    def save(self, world):
        overview = {}
        overview["world"] = world
        for realm in REALMS:
            overview[realm] = {}
            extforts = []
            for fort in self.realm[realm].forts:
                if fort[0] not in FORTS[realm] and not f"{realm} Great Wall" in fort[0]:
                    extforts.append(fort)
            
            overview[realm]["extforts"] = extforts
            overview[realm]["gems"] = self.realm[realm].gems
            overview[realm]["vulnerable"] = self.realm[realm].is_vulnerable
            overview[realm]["invaded"] = self.realm[realm].is_invaded
            overview[realm]["last_wish"] = self.realm[realm].last_wish
        
        with open(f"{CACHE_DIR}/{world}.yaml", "w") as file:
            dump(overview, file, default_flow_style=False)
            
            
def broadcast(output, webhooks):
    for webhook in webhooks:
        for line in output:
            post(webhook, dict(content=line))


def log(message: str, incl_time=False):
    if incl_time:
        print(f"{datetime.utcnow().replace(microsecond=0)}: {message}")
    else:
        print(message)


def output_changes(emojis, changes):
    output_line = []
    for event in changes:
        if event[0] == "cap":
            realm, fort, fortrealm = event[1:]
            output_line.append(f"{emojis['flag'][realm]} **{realm}** has captured **{fort}** {emojis['fort'][fortrealm]}")
        elif event[0] == "rec":
            realm = event[1]
            output_line.append(f"{emojis['flag'][realm]} **{realm} has recovered its Great Wall** {emojis['flag'][realm]}")
        elif event[0] == "inv":
            w_realm, l_realm = event[1], event[3]
            output_line.append(f"{emojis['flag'][w_realm]} **{w_realm} starts invading {l_realm}** {emojis['flag'][l_realm]}")
        elif event[0] == "vuln":
            realm = event[1]
            output_line.append(f"{emojis['flag'][realm]} **{realm} is vulnerable** :warning:")
        elif event[0] == "gem":
            w_realm, gem, index, l_realm = event[1:]
            output_line.append(f"{emojis['flag'][w_realm]} **{w_realm}** has taken {emojis['gem'][gem]} **{gem} gem #{index}** from **{l_realm}** {emojis['flag'][l_realm]}")
        elif event[0] == "boss":
            boss = event[1]
            output_line.append(f"{emojis['boss'][boss]} **{boss} has respawned**")
        elif event[0] == "wish":
            realm = event[1]
            output_line.append(f"{emojis['flag'][realm]} **{realm} made a dragon wish** :dragon:")

    return(output_line)


def watch_changes(firstrun=False):
    global NEXT_RUN
    NEXT_RUN += timedelta(minutes=1)
    loop.enterabs(NEXT_RUN.timestamp(),1,watch_changes)

    for world in WORLDS:

        try:
            r = get(f"https://www.championsofregnum.com/index.php?l=1&ref=gmf&sec=3&world={world}", timeout=10)
            assert r.status_code == 200
        except:
            log("Bad/No Response", True)
            continue
            
        try:
            assert cache[world]
        except KeyError:
            cache[world] = Cache()
        
        cache[world].fill(r)
        
        try:
            assert romap[world]
        except KeyError:
            if cache[world].parse():
                romap[world] = Map(cache[world])
            else:
                log("Bad data", True)
                continue
                
        if firstrun:
            log(f"First run for {world.upper()}", True)
            cache[world].swap()
            romap[world].save(world)
            continue
                
        if cache[world].has_changed():
            if not cache[world].parse():
                log("Bad data", True)
                continue
            
            changes = cache[world].get_changes()
            events = romap[world].update(changes)
            
            log(f"- {world.upper()} -", True)
            log(events)
            
            for server in config.servers:
                if world in server.webhooks:
                    output = output_changes(server.emojis, events)
                    broadcast(output, [server.webhooks[world]])

            cache[world].swap()
            romap[world].save(world)


# Bosses stuff
def boss_check(firstrun=False):
    for boss in BOSSES:
        modified = False
        respawn = datetime.strptime(config.bosstimes[boss], "%d/%m/%Y %H:%M")
        cur_date = datetime.utcnow()

        # Calculate and save new respawn time
        while respawn <= cur_date:
            if not modified: 
                modified = True
            respawn += timedelta(hours=109)

        if modified:
            config.update_boss(boss, respawn)
            if not firstrun:
                log(f"{boss} has respawned", True)
                for server in config.servers:
                    output = output_changes(server.emojis, [("boss", boss)])
                    broadcast(output, server.webhooks)
        
        if firstrun or (not firstrun and modified):
            loop.enterabs(respawn.timestamp(),1,boss_check)


config = Config()
config.load()
romap = {}
cache = {}

loop = scheduler(time, sleep)

boss_check(True)
watch_changes(True)

loop.run()
